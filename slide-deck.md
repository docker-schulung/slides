---
marp: true
paginate: true
theme: my-theme
title: Docker - Der komplette Einstieg
---

# Docker Grundkurs - Der komplette Einstieg

---

# Wer bin ich?

**Name:** André Schreck
**Alter:** 38 Jahre
Eigentlich aus Bremen, wohne jetzt Oldenburg
- Seit >15 Jahren in der Softwareentwicklung tätig
  - Seit 2020 als Freelancer selbstständig

---

# Inhalt

---

## Tag 1

- Kennenlernen
- Historie und Funktionsweise von Docker
- Images
- Container
- Volumes

---

## Tag 2

- Containerisierung eigener Software Applikationen
- Netzwerke und Docker
- Security
- Docker Registries

---

## Tag 3

- Service-Architekturen mit Docker Compose abbilden
- Clustering

---

# Vorstellungsrunde

- Wer seid ihr?
- Was ist euer Background?
- Was ist eure Motivation?

---

# Was ist das Ziel?

Am Ende von Tag 2 sollten wir drei containerisierte Applikationen haben, die aufeinander zugreifen können

![bg right contain](img/services.png)

---

# Historie

- Wir schreiben das Jahr 2011
- Anwendungen wurden größtenteils im eigenen Rechenzentrum betrieben oder auf gemieteten VMs
- "It runs on my machine"
- dotCloud fängt an, an Containerisierung zu arbeiten
- 2013 wird Docker veröffentlicht und dotCloud wird zu Docker Inc.

---

# Funktionsweise von Docker

---

## Docker vs VM

- Kein Overhead durch emulierte Hardware
- Container teilen sich den Kernel des Host Betriebssystems
  - Bessere Performance
  - Macht den Host Kernel angreifbarer

![bg right contain](img/container-vs-vm.png)

---

## Wie macht Docker das?

- cgroups
  - Quotas für Prozesse auf CPU und RAM vergeben
- namespaces
  - Einem Prozess wird vorgegaukelt er wäre der einzige (PID, Netzwerk, Dateisystem)
- cgroups und namespaces kombiniert isolieren einen Prozess von allen anderen
- VM simuliert Hardware
  - Installation eines anderen Betriebssystems möglich (das kann docker nicht)

---

## Alternativen zu Docker

- Die Linux Foundation hat mit der Open Container Initiative ein standardisiertes Image und Container Format herausgebracht
- Alternative Tools sind entstanden
  - **containerd:** Eine Container-Runtime der CNCF
  - **Podman:** Die "sichere" Alternative zu Docker

<!--
Kubernetes, CRI
-->

---

## Aufgabe

- Installiert docker auf euren Systemen
- Führt das hello-world image aus: `docker run hello-world`

---

# Images und Container

---

## Begrifflichkeit

- Image
  - Speicherabbild für einen Container
  - Ein Image besteht aus Schichten (Layer)
- Container
  - Lauffähige Instanz eines Images
  - Auf Basis eines Images können mehrer Container erzeugt werden

---

## Docker Kommandos

- `docker pull`
- `docker images`
- `docker run`
- `docker ps`
- `docker rm` und `docker rmi` bzw. `docker image rm`
- `docker start` und `docker stop`
- `docker build`
- `docker push`

---

## `docker pull`

- Lädt ein Image herunter: `docker pull <image>`
  - Z.B: `docker pull ubuntu`
- Wird eher selten ausgeführt (`docker run` erledigt das implizit mit)

---

## `docker images`

- Zeigt die lokal vorhandenen Images an
- Alternativ `docker image ls`

---

## `docker run`

- Erzeugt einen Container auf Basis eines Images

```shell
# Erzeugt auf Basis des Ubuntu Images einen Container und startet ihn
docker run ubuntu

# Startet ebenfalls einen Ubuntu Container und führt das Kommando bash aus
docker run -it ubuntu bash
```

---

## `docker ps`

- Zeigt die laufenden Container an
- Container läuft bis der Prozess beendet wird
  - Alles was innerhalb des Containers angelegt wurde, wird gelöscht
- Wo komment die Images denn her?
  - Image Repositories (hub.docker.com)

---

## Löschen von Images und Containern

```shell
# Löscht einen Container bzw. erzeugt einen Fehler falls er noch läuft
docker rm <container>

# Löscht ein Image bzw. erzeugt einen Fehler falls es noch einen Container zu dem Image gibt
docker rmi <image>
# oder
docker image rm <image>
```

---

## Aufgabe

- Startet den Super Mario retro container https://hub.docker.com/r/pengbai/docker-supermario/
- Spielt zur Belohnung das erste Level
- Löscht danach den Container und das Image
<!--docker run -d --name mario -p 8090:8080 pengbai/docker-supermario-->

---

## 3rd-party Images

- Es gibt diverse Applikationen schon als fertige Docker Images
  - **Webserver:** Apache, nginx ...
  - **Datenbanken:** MySQL, MongoDB ...
  - u.v.m.

<!--
Apacke: docker run -d --name web -p 8080:80 -v /home/schreck/docker-workshop/web-content:/usr/local/apache2/htdocs/ httpd:2.4
MongoDB: docker run --name db -p 27017:27017 -d mongo:latest
nginx: docker run --name web -d nginx
-->

---

## Weitere häufig genutzte Kommandos

- **Stoppen eines Containers:** `docker stop <container>` / `docker kill <container>`
- **Starten eines Containers:** `docker start <container>`
- **Logs ausgeben:** `docker logs [--follow] <container>`
- **Löschen eines Containers:** `docker rm <container>`
- **Löschen eines Images:** `docker rmi <image>` bzw. `docker image rm <image>`
- **Alles löschen, was nicht benutzt wird:** `docker system prune --all --volumes`

---

## Ports 

- Jeder Container bekommt über namespaces sein eigenes Netzwerk
  - Dem Container wird vorgegaukelt er wäre alleine
- Ports im Container müssen auf Ports des Hosts gemappt werden

```shell
docker run -d --name web -p 8080:80 httpd:2.4
```

---

## Aufgabe

- Erzeugt einen nginx Container
- Ruft den nginx in einem Browser auf

---

## Volumes und Bind mounts

---

# Bind mounts

- Verzeichnis auf dem Host in den Container mounten
```shell
docker run -d --name web -p 8080:80 -v <absoluter Pfad>:/usr/local/apache2/htdocs/ httpd:2.4
```

--- 

## Volumes

- **Volume erstellen:** Z.B. `docker volume create uploads`
  - Volume mit dem Namen `uploads` wird in den Container gemountet
  - Volumes werden von docker gemanaged
  - Volumes können sowohl im Dockerfile als auch bei der Instanziierung des Container gemappt werden

```shell
docker run -d --name web -p 8080:80 -v <volume name>:/usr/local/apache2/htdocs/ httpd:2.4
```

---

## Aufgabe

- Erzeugt ein Verzeichnis mit einer `index.html` Datei
- Mountet das Verzeichnis in euren nginx Container
- Stellt sicher, dass euer index.html angezeigt wird
- Erzeugt, während der Container läuft, eine zweite HTML Datei und ruft diese über den nginx im Browser auf

<!--docker run -d --name web -p 8080:80 -v /home/schreck/docker-workshop/web-content:/usr/share/nginx/html nginx-->

---

# Containerisierung einer eigenen Software Applikation

---

## Das Dockerfile

- Kochrezept für das Image
  - **FROM:** Definiert das base image
  - **RUN:** Führt Befehle zur Build-time des Images aus
  - **ADD & Copy:** Fügt Dateien zum Image hinzu
  - **CMD und ENTRYPOINT:** Auszuführender Befehl beim Start eines Containers
- https://docs.docker.com/engine/reference/builder/

---

## Image bauen

```shell
docker build [Options] .
```

- User wechseln (nicht root)
- WORKDIR
- User beim ADD Befehl (ADD --chown=<user>:<group> ...)
- Jedes RUN läuft in einer eigenen shell
- .dockerignore
```shell
docker build -t <name>:<version> <dir>
```

--- 

## Layer

- Imutable
- Klarsichtfolien
- Caching
  - Reihenfolge der Befehle beachten um caching optimal auszunutzen (Beispiel npm install)
- Ermöglicht Wiederverwendung in verschiedenen Images

---

## Aufgabe

- Erzeugt für den article-service und den price-service ein `Dockerfile`
  - **article-service**: `git clone https://gitlab.com/docker-schulung/article-service.git`
  - **price-service**: `git clone https://gitlab.com/docker-schulung/price-service.git`
- Erzeugt daraus Docker images
- Erzeugt aus den Images Container und prüft, dass sie Artikel bzw. Preise zurückgeben
  - **article-service**: `http://localhost:<PORT>/articles`
  - **price-service**: `http://localhost:<PORT>/prices`
- Übergebt einen anderen Port mit Hilfe der Umgebungsvariable `PORT`
<!--
---

- Eigenes Image instanziieren
  - Neustarts (--restart)
- Image pushen
-->
---

# Sicherheit von 3rd Party Images

- Das richtige base image verwenden (Docker Official Image)
- Vulnerability Checks: z.B. mit `docker scout`
- Multi-stage builds

---

## Docker Hub

- Dockers offizielle Registry
- Jeder kann Images pushen

---

## Multi-stage builds

- compile in einem image
- run in einem anderen und das kompilierergebnis kopieren
```shell
FROM maven AS builder
WORKDIR /home
ADD . .
RUN mvn clean package -D skipTests
#--------------------
FROM openjdk:17-alpine
WORKDIR /home
COPY --from=builder /home/target/movie-service-0.0.1-SNAPSHOT.jar ./app.jar
CMD ["java", "-jar", "app.jar"]

```
- Das `builder`-Image kompiliert die Applikation
  - Sourcecode und maven werden benötigt
- Zur Laufzeit wird als Basis nur Java und das gebaute jar benötigt

---

## Aufgabe

- Baut die Dockerfiles so um, dass Go und der Sourcecode zur Laufzeit nicht mit ausgeliefert werden
  - Versucht die Images so klein wie möglich zu bekommen

---

# Log Management

- Abfragen der Standardausgabe (STDOUT und STDERR)

```shell
docker logs <container>
```

---

## Aufgabe

- Es besteht die Möglichkeit `article-service` und `price-service` mit Daten zu befüllen
  - **article-service:** `/usr/local/article-service/data.json`
  - **price-service:** `/usr/local/price-service/data.json`
- Mountet Daten in die Container

Falls sich etwas nicht so verhält wie erwartet, schaut mal ins Log.

---

# Netzwerke

---

## Warum überhaupt

- Über namespaces wird den Containern vorgegaukelt, dass er alleine wäre
- Mit Hilfe von Netzwerken können wir Kommunikation zwischen den Containern ermöglichen

---

## Netzwerk-Typen

- bridge
- host
- overlay
- ipvlan
- macvlan
- none

---

## Host Netzwerk

- Hebt die Isolierung des Containernetzwerks auf
  - Applikation binden sich also direkt an den Port auf dem Host
  - `-p` hat keinen Effekt
- Vorsicht: Horcht eine Applikation auf Port 80, ist sie über die IP des Hosts auch auf Port 80 erreichbar
- Funktioniert nur unter Linux und nicht mit Docker Desktop for Mac und Windows

---

## Overlay Netzwerk

- Wird genutzt wenn sich das Netzwerk über mehrere Hosts erstrecken soll
  - Z.B. in einem Docker Swarm Szenario

---

## Bride Netzwerke

- Der default Netzwerktyp

```shell
# Netzwerk erstellen
docker network create <name>

# DB mit Netzwerk verbinden
docker run --name movie-db -d --network movie-network --network-alias db mongo:latest

# App mit Netzwerk verbinden
docker run --name movie-service -p 8080:8080 -d --network movie-network -e "DATABASE_URI=mongodb://db:27017/movies" movie-service:0.0.1

# Container nachträglich zum Netzwerk hinzufügen
docker network connect <network> <container>
```

---

## Aufgabe (Teil 1)

- Klont den `frontend-service` und containerisiert ihn
  - `git clone https://gitlab.com/docker-schulung/frontend-service.git`
  - Der `frontend-service` unterstützt drei Umgebungsvariablen
    - `ARTICLE_SERVICE_HOST`
    - `PRICE_SERVICE_HOST`
    - `PORT` (optional)

---

## Aufgabe (Teil 2)

- Erzeugt ein Netzwerk und fügt `frontend-service`, `article-service` und `price-service` zum Netzwerk hinzu, damit sie miteinander kommunizieren können
- Nur der `frontend-service` sollte exposed sein
- Prüft, dass beim Aufruf des `frontend-service` die aggregierten Daten ausgegeben werden

---

## Aufgabe (Teil 3)

- Erzeugt ein zweites Netzwerk und sorgt dafür, dass `article-service` und `price-service` nicht aufeinander zugreifen können
  - **Netzwerk 1:** `frontend-service` und `article-service`
  - **Netzwerk 2:** `frontend-service` und `price-service`

---

# Docker compose

---

## Servicebeschreibung

https://docs.docker.com/compose/compose-file/

```shell
services:
  db:
    image: mongo:latest
  api:
    build: .
    ports:
      - "8080:8080"
    environment:
      - DATABASE_URI=mongodb://db:27017/movies
    depends_on:
      - db
```

---

## Kommandos

- `docker-compose up`
- `docker-compose down`
- `docker-compose start`
- `docker-compose stop`
- `docker-compose ps`
- `docker-compose images`
- `docker-compose rm / kill`

---

## Aufgabe

- Erstellt eine `docker-compose.yml` in der die Services `article-service`, `price-service` und `frontend-service` erzeugt werden

---

## Volumes in Docker compose

---

### Bind mounts

```shell
services:
  db:
    image: mongo:latest
    volumes:
      - ./db-data:/data/db
  api:
    build: .
    ports:
      - "8080:8080"
    environment:
      - DATABASE_URI=mongodb://db:27017/movies
    depends_on:
      - db
```

---

## Volumes

```shell
services:
  db:
    image: mongo:latest
    volumes:
      - db-data:/data/db
  api:
    build: .
    ports:
      - "8080:8080"
    environment:
      - DATABASE_URI=mongodb://db:27017/movies
    depends_on:
      - db


volumes:
  db-data:
```

---

## Aufgabe

- Fügt die Volumes mit den JSON Daten in eure `docker-compose.yml` hinzu

---

## Profile in Docker compose

- Services können Profilen zugeordnet werden
- Darüber lässt sich steuern welche Services beim `docker-compose up` gestartet werden
- `docker-compose --profile <profile> up`

```shell
services:
  db:
    image: mongo:latest
    profiles: ["prod"]
  api:
    build: .
    ports:
      - "8080:8080"
    environment:
      - DATABASE_URI=mongodb://db:27017/movies
    depends_on:
      - db
    profiles: ["prod"]
```

---

## Aufgabe

- Erzeugt vom `article-service` und `price-service` jeweils zwei Services
  - Der eine Service soll das Volume verwenden, der andere nicht
- Fügt die Profile `dev` und `prod` hinzu
  - **prod:** nutzt die Volumes
  - **dev:** nutzt die Volumes nicht

---

## Netzwerke in Docker compose

```shell
services:
  db:
    image: mongo:latest
    networks:
      - my-network
    volumes:
      - db-data:/data/db
  api:
    build: .
    ports:
      - "8080:8080"
    networks:
      - my-network
    environment:
      - DATABASE_URI=mongodb://db:27017/movies
    depends_on:
      - db

networks:
  my-network:
```

---

## Aufgabe

- Sorgt in der `docker-compose.yml` dafür, dass `article-service` und `price-service` in unterschiedlichen Netzwerken sind

---

## Health Checks

```shell
services:
  serviceA:
    build: ./serviceA
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8080/health"]
      interval: 5s
      retries: 3
  gateway:
    build: ./gateway
    ports:
      - "8080:8080"
    environment:
      - SERVICE_A_HOST=http://serviceA:8080
    depends_on:
      serviceA:
        condition: service_healthy
        restart: true
```

**Achtung:** Der Request für den Healthcheck kommt aus dem Docker Container in dem die zu checkende Anwendung läuft. `curl` muss in dem Container also installiert sein.

---

## Skalierung

```shell
services:
  serviceA:
    build: ./serviceA
    deploy:
      mode: replicated
      replicas: 3
  gateway:
    build: ./gateway
    ports:
      - "8080:8080"
    environment:
      - SERVICE_A_HOST=http://serviceA:8080
```

---

# Betreiben einer eigenen Docker Registry

---

## Warum eigentlich?

- Einsatz ohne Zugang zum Internet
- Unternehmensinterner Einsatz
- Sourcecode und Container zusammen verwalten

---

## Docker registry als docker container

- Docker registry als Container starten
```shell
docker run -d -p 5000:5000 --restart always --name docker-registry registry:latest
```

- Image pushen
```shell
docker tag <image> localhost:5000/<image>
docker push localhost:5000/<image>
``` 

---

## Weitere Möglichkeiten

- Github Container Registry
- Gitlab
- NEXUS
- Artifactory

--- 

## Aufgabe

- Setzt eine Docker Registry bei euch lokal auf
- Verteilt die `frontend-service`, `article-service` und `price-service` Images auf eure Registries, so dass bei keinem alle Services vorhanden sind
- Führt ein `docker system prune --all --volumes` aus (während der Registry Container läuft)
- Startet bei euch die Container auf Basis der Images in euren Docker Registries

Damit ihr per http auf die Registry des jeweils anderen zugreifen könnt muss in der daemon config `insecure-registries` gesetzt werden:
`z.B: "insecure-registries": ["W02:5000"]`

---

# Docker Swarm

https://docs.docker.com/engine/swarm/

- Cluster management
- Declarative service model
- Scaling & Load balancing
- Desired state reconciliation
- Rolling updates

---

## Akteure

- Manager nodes
- Worker nodes
- Services und Tasks

---

## Manager nodes

- Sorgen für den korrekten Zustand im Cluster
- Scheduling

---

## Worker nodes

- Worker nodes bekommen von Manager nodes ihre Aufgaben (Tasks)
- Worker nodes berichten ihren Status an die Manager nodes

---

## Services und Tasks

---

## Tasks

- Die kleinste Einheit, die es in Swarm gibt
- Tasks werden von Docker Containern ausgführt

---

## Services

- Service beinhalten die Konfiguration einer Menge von Tasks
  - Image
  - Replicas
  - Port Mappings
  - ...

---

## Swarm erstellen

```shell
docker swarm init --advertise-addr <HOST-IP>
```

Das Kommando gibt ein token zurück mir dem sich Worker-Nodes an- und abmelden können

```shell
docker swarm join --token <TOKEN> <HOST>

docker swarm leave
```

---

## Service erzeugen

```shell
docker service create --name my-service --replicas 5 -p 8080:8080 <image>
```

---

## Update eines Services

```shell
docker service update --replicas 5 --publish-rm 8080:8080 --publish-add 5000:8080 <service>
```

---

## Aufgabe

- Erzeugt euch mit `docker swarm init` einen Swarm
- Erzeugt euch einen Service (auf Basis des `article-service` oder `price-service`)
- Ändert per Update den Port (oder etwas anderes, was ihr ändern wollt `docker update --help`)

https://docs.docker.com/engine/swarm/swarm-tutorial/

---

## Stack files

- Analog zu Docker compose V2
  - Beim Service muss ein Image hinterlegt sein, build wird ignoriert
- Erstellen und Update des Stacks über denselben Befehl

```shell
docker stack deploy -c <compose.yml> <name>
```

---

## Aufgabe

- Erzeugt euch auf Basis eurer `docker-compose.yml` eine `shop-stack.yml`
- Deployed den Stack in euren Swarm
- Nehmt Änderungen an der yml vor und deployed erneut

https://docs.docker.com/engine/swarm/stack-deploy/

---

# Docker machine

---

```shell
docker-machine create -d virtualbox <name>

docker-machine ip <name>

docker-machine env <name>
```

---

## Aufgabe

Das machen wir zusammen
<!--
---

## Docker Swarm vs. Kubernetes
-->